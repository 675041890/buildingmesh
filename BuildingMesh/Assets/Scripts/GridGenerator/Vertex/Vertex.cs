using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Vertex
{
    /// <summary>
    /// 每个点在世界中的实际初始位置
    /// 为后续网格平滑做准备
    /// </summary>
    public Vector3 initialPosition;

    public Vector3 currentPosition;

    public Vector3 offset = Vector3.zero;

    public List<Vertex_Y> vertex_Ys = new List<Vertex_Y>();

    public bool isBoundary;

    public int index;

    public List<SubQuad> subQuads = new List<SubQuad>();

    public void Relax()
    {
        currentPosition = initialPosition + offset;
    }

    public void BoundaryCheck()
    {
        //判断是否为边缘
        bool isBoundaryHex = this is Vertex_Hex && ((Vertex_Hex)this).Coord.radius == Grid.radius;
        //判断是否为上下界
        bool isBoundaryMid = this is Vertex_Mid && ((Vertex_Mid)this).edge.hexes.ToArray()[0].Coord.radius == Grid.radius
            && ((Vertex_Mid)this).edge.hexes.ToArray()[1].Coord.radius == Grid.radius;
        isBoundary = isBoundaryHex || isBoundaryMid;
    }

    public Mesh CreateMesh()
    {
        List<Vector3> meshVertices = new List<Vector3>();
        List<int> meshTriangles = new List<int>();

        foreach (SubQuad subQuad in subQuads)
        {
            if (this is Vertex_Center)
            {
                meshVertices.Add(currentPosition);
                meshVertices.Add(subQuad.GetMid_cd());
                meshVertices.Add(subQuad.GetCenterPosition());
                meshVertices.Add(subQuad.GetMid_bc());
            }
            else if(this is Vertex_Mid)
            {
                if (subQuad.b == this)
                {
                    meshVertices.Add(currentPosition);
                    meshVertices.Add(subQuad.GetMid_bc());
                    meshVertices.Add(subQuad.GetCenterPosition());
                    meshVertices.Add(subQuad.GetMid_ab());
                }
                else
                {
                    meshVertices.Add(currentPosition);
                    meshVertices.Add(subQuad.GetMid_ad());
                    meshVertices.Add(subQuad.GetCenterPosition());
                    meshVertices.Add(subQuad.GetMid_cd());
                }
            }
            else
            {
                meshVertices.Add(currentPosition);
                meshVertices.Add(subQuad.GetMid_ab());
                meshVertices.Add(subQuad.GetCenterPosition());
                meshVertices.Add(subQuad.GetMid_ad());
            }
        }

        for (int i = 0; i < meshVertices.Count; i++)
        {
            meshVertices[i] -= currentPosition;
        }
        for (int i = 0; i < subQuads.Count; i++)
        {
            meshTriangles.Add(i * 4);
            meshTriangles.Add(i * 4 + 1);
            meshTriangles.Add(i * 4 + 2);
            meshTriangles.Add(i * 4);
            meshTriangles.Add(i * 4 + 2);
            meshTriangles.Add(i * 4 + 3);
        }

        Mesh mesh = new Mesh();
        mesh.vertices = meshVertices.ToArray();
        mesh.triangles = meshTriangles.ToArray();
        return mesh;
    }
}



