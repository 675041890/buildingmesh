using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertex_Y 
{
    public readonly string name;
    public readonly Vertex vertex;
    public readonly int y;
    public readonly Vector3 worldPosition;
    public readonly bool isBoundary;
    public bool isActive;
    public List<SubQuad_Cube> subQuad_Cubes = new List<SubQuad_Cube>();

    public Vertex_Y(Vertex vertex,int y)
    {
        this.name = "Vertex_" + vertex.index + "_" + y;
        this.vertex = vertex;
        this.y = y;
        isBoundary = vertex.isBoundary || y == Grid.height || y == 0;
        worldPosition = vertex.currentPosition + Vector3.up * (y * Grid.cellHeight);
    }
}
