using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertex_TriangleCenter : Vertex_Center
{
    public Vertex_TriangleCenter(Triangle triangle)
    {
        initialPosition = (triangle.a.initialPosition + triangle.b.initialPosition + triangle.c.initialPosition) / 3;
    }
}
