using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertex_Center : Vertex
{
    public List<Mesh> CreateSideMesh()
    {
        int n = this.subQuads.Count;
        List<Mesh> meshes = new List<Mesh>();
        for (int i = 0; i < n; i++)
        {
            List<Vector3> meshVertices = new List<Vector3>();
            List<int> meshTriangles = new List<int>();

            meshVertices.Add(subQuads[i].GetCenterPosition()+Vector3.up * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[i].GetMid_cd()+Vector3.up * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[(i + n - 1) % n].GetCenterPosition()+Vector3.up * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[i].GetCenterPosition()+Vector3.down * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[i].GetMid_cd()+Vector3.down * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[(i + n - 1) % n].GetCenterPosition() + Vector3.down * Grid.cellHeight / 2);

            for (int j = 0; j < meshVertices.Count; j++)
            {
                meshVertices[j] -= currentPosition;
            }

            meshTriangles.Add(0);
            meshTriangles.Add(1);
            meshTriangles.Add(3);
            meshTriangles.Add(1);
            meshTriangles.Add(4);
            meshTriangles.Add(3);
            meshTriangles.Add(1);
            meshTriangles.Add(2);
            meshTriangles.Add(5);
            meshTriangles.Add(1);
            meshTriangles.Add(5);
            meshTriangles.Add(4);

            Mesh mesh = new Mesh();
            mesh.vertices = meshVertices.ToArray();
            mesh.triangles = meshTriangles.ToArray();
            meshes.Add(mesh);
        }
        return meshes;
    }
}
