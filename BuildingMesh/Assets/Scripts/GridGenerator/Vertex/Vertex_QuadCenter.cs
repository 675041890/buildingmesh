using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertex_QuadCenter : Vertex_Center
{
    public Vertex_QuadCenter(Quad quad)
    {
        initialPosition = (quad.a.initialPosition + quad.b.initialPosition + quad.c.initialPosition + quad.d.initialPosition) / 4;
    }
    
}
