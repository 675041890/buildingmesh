using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 六边形点阵
/// </summary>
public class Vertex_Hex : Vertex
{
    /// <summary>
    /// 六边形点阵的点
    /// </summary>
    public readonly Coord Coord;

    public Vertex_Hex(Coord coord)
    {
        this.Coord = coord;
        initialPosition = coord.CoordConvertWorldPositon();
        currentPosition = initialPosition;
    }

    /// <summary>
    /// 获取构成六边形点阵的点
    /// </summary>
    /// <param name="vertices"></param>
    /// <param name="radius"></param>
    public static void Hex(List<Vertex_Hex> vertices,int radius)
    {
        foreach (var coord in Coord.Coord_Hex(radius))
        {
            vertices.Add(new Vertex_Hex(coord));
        }
    }

    /// <summary>
    /// 获取构成六边形点阵的环
    /// </summary>
    /// <param name="radius"></param>
    /// <param name="vertices"></param>
    /// <returns></returns>
    public static List<Vertex_Hex> GrabRing(int radius,List<Vertex_Hex> vertices)
    {
        if (radius == 0)
        {
            return vertices.GetRange(0,1);
        }

        return vertices.GetRange(radius * (radius - 1 ) * 3 + 1,radius * 6) ;
    }

    public List<Mesh> CreateSideMesh()
    {
        int n = this.subQuads.Count;
        List<Mesh> meshes = new List<Mesh>();
        for (int i = 0; i < n; i++)
        {
            List<Vector3> meshVertices = new List<Vector3>();
            List<int> meshTriangles = new List<int>();

            meshVertices.Add(subQuads[i].GetCenterPosition() + Vector3.up * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[i].GetCenterPosition() + Vector3.down * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[i].GetMid_ab() + Vector3.up * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[i].GetMid_ab() + Vector3.down * Grid.cellHeight / 2);

            foreach (SubQuad subQuad in subQuads)
            {
                if (subQuad.d == subQuads[i].b)
                {
                    meshVertices.Add(subQuad.GetCenterPosition() + Vector3.up * Grid.cellHeight / 2);
                    meshVertices.Add(subQuad.GetCenterPosition() + Vector3.down * Grid.cellHeight / 2);
                    break;
                }
            }

            for (int j = 0; j < meshVertices.Count; j++)
            {
                meshVertices[j] -= currentPosition;
            }

            meshTriangles.Add(0);
            meshTriangles.Add(2);
            meshTriangles.Add(1);
            meshTriangles.Add(2);
            meshTriangles.Add(3);
            meshTriangles.Add(1);
            meshTriangles.Add(2);
            meshTriangles.Add(4);
            meshTriangles.Add(5);
            meshTriangles.Add(2);
            meshTriangles.Add(5);
            meshTriangles.Add(3);

            Mesh mesh = new Mesh();
            mesh.vertices = meshVertices.ToArray();
            mesh.triangles = meshTriangles.ToArray();
            meshes.Add(mesh);
        }
        return meshes;
    }
}
