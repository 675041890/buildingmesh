using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Edge类的中点
/// </summary>
public class Vertex_Mid : Vertex
{
    public readonly Edge edge;
    public Vertex_Mid(Edge edge,List<Vertex_Mid> mids)
    {
        this.edge = edge;
        Vertex_Hex a = edge.hexes.ToArray()[0];
        Vertex_Hex b = edge.hexes.ToArray()[1];
        initialPosition = (a.initialPosition + b.initialPosition) / 2;

        mids.Add(this);
    }

    public List<Mesh> CreateSideMesh()
    {
        List<Mesh> meshes = new List<Mesh>();
        for (int i = 0; i < 4; i++)
        {
            List<Vector3> meshVertices = new List<Vector3>();
            List<int> meshTriangles = new List<int>();

            meshVertices.Add(subQuads[i].GetCenterPosition() + Vector3.up * Grid.cellHeight / 2);
            meshVertices.Add(subQuads[i].GetCenterPosition() + Vector3.down * Grid.cellHeight / 2);

            if (subQuads[i].b == this)
            {
                meshVertices.Add(subQuads[i].GetMid_bc() + Vector3.up * Grid.cellHeight / 2);
                meshVertices.Add(subQuads[i].GetMid_bc() + Vector3.down * Grid.cellHeight / 2);
                foreach (SubQuad subQuad in subQuads)
                {
                    if (subQuad.c == subQuads[i].c && subQuad != subQuads[i])
                    {
                        meshVertices.Add(subQuad.GetCenterPosition() + Vector3.up * Grid.cellHeight / 2);
                        meshVertices.Add(subQuad.GetCenterPosition() + Vector3.down * Grid.cellHeight / 2);
                        break;
                    }
                }

            }
            else
            {
                meshVertices.Add(subQuads[i].GetMid_ad() + Vector3.up * Grid.cellHeight / 2);
                meshVertices.Add(subQuads[i].GetMid_ad() + Vector3.down * Grid.cellHeight / 2);
                foreach (SubQuad subQuad in subQuads)
                {
                    if (subQuad.a == subQuads[i].a && subQuad != subQuads[i])
                    {
                        meshVertices.Add(subQuad.GetCenterPosition() + Vector3.up * Grid.cellHeight / 2);
                        meshVertices.Add(subQuad.GetCenterPosition() + Vector3.down * Grid.cellHeight / 2);
                        break;
                    }
                }
            }

            for (int j = 0; j < meshVertices.Count; j++)
            {
                meshVertices[j] -= currentPosition;
            }

            meshTriangles.Add(0);
            meshTriangles.Add(2);
            meshTriangles.Add(1);
            meshTriangles.Add(2);
            meshTriangles.Add(3);
            meshTriangles.Add(1);
            meshTriangles.Add(2);
            meshTriangles.Add(4);
            meshTriangles.Add(5);
            meshTriangles.Add(2);
            meshTriangles.Add(5);
            meshTriangles.Add(3);

            Mesh mesh = new Mesh();
            mesh.vertices = meshVertices.ToArray();
            mesh.triangles = meshTriangles.ToArray();
            meshes.Add(mesh);
        }
        return meshes;
    }
}
