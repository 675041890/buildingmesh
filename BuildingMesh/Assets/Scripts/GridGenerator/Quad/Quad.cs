using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quad 
{
    public readonly Vertex_Hex a;
    public readonly Vertex_Hex b;
    public readonly Vertex_Hex c;
    public readonly Vertex_Hex d;

    public readonly Edge ab;
    public readonly Edge bc;
    public readonly Edge ad;
    public readonly Edge cd;

    public readonly Vertex_QuadCenter center;

    public Quad(Vertex_Hex a, Vertex_Hex b, Vertex_Hex c, Vertex_Hex d,List<Edge> edges,List<Quad> quads, List<Vertex_Center> centers)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;

        ab = Edge.FindEdge(a,b,edges);
        ad = Edge.FindEdge(a,d,edges);
        bc = Edge.FindEdge(b,c,edges);
        cd = Edge.FindEdge(c,d,edges);

        center = new Vertex_QuadCenter(this);
        centers.Add(center);

        quads.Add(this);
    }

    public void SubDivide(List<SubQuad> subQuads)
    {
        SubQuad quad_a = new SubQuad(a, ab.mid, center, ad.mid, subQuads);
        SubQuad quad_b = new SubQuad(b, bc.mid, center, ab.mid, subQuads);
        SubQuad quad_c = new SubQuad(c, cd.mid, center, bc.mid, subQuads);
        SubQuad quad_d = new SubQuad(d, ad.mid, center, cd.mid, subQuads);
        a.subQuads.Add(quad_a);
        b.subQuads.Add(quad_b);
        c.subQuads.Add(quad_c);
        d.subQuads.Add(quad_d);

        center.subQuads.Add(quad_a);
        center.subQuads.Add(quad_b);
        center.subQuads.Add(quad_c);
        center.subQuads.Add(quad_d);

        ab.mid.subQuads.Add(quad_a);
        ab.mid.subQuads.Add(quad_b);

        ad.mid.subQuads.Add(quad_a);
        ad.mid.subQuads.Add(quad_d);

        cd.mid.subQuads.Add(quad_c);
        cd.mid.subQuads.Add(quad_d);

        bc.mid.subQuads.Add(quad_c);
        bc.mid.subQuads.Add(quad_b);


    }
}
