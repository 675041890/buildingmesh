using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �����εı�
/// </summary>
public class Edge
{
    public readonly HashSet<Vertex_Hex> hexes;

    public Vertex_Mid mid;

    public Edge(Vertex_Hex a,Vertex_Hex b,List<Edge> edges,List<Vertex_Mid> mids)
    {
        hexes = new HashSet<Vertex_Hex> { a, b };
        mid = new Vertex_Mid(this,mids);
        edges.Add(this);
    }

    public static Edge FindEdge(Vertex_Hex a,Vertex_Hex b,List<Edge> edges)
    {
        foreach (Edge edge in edges)
        {
            if (edge.hexes.Contains(a) && edge.hexes.Contains(b))
            {
                return edge;
            }
        }
        return null;
    }
}
