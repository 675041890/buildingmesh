using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubQuad_Cube
{

    public readonly SubQuad subQuad;

    public readonly int y;

    public readonly Vector3 centerPosition;

    public readonly Vertex_Y[] vertex_Ys = new Vertex_Y[8];

    public string bit = "00000000";
    public string pre_bit = "00000000";
    public SubQuad_Cube(SubQuad subQuad,int y)
    {
        this.subQuad = subQuad;
        this.y = y;
        centerPosition = subQuad.GetCenterPosition() + Vector3.up * Grid.cellHeight * (y + 0.5f);
        vertex_Ys[0] = subQuad.a.vertex_Ys[y + 1];
        vertex_Ys[1] = subQuad.b.vertex_Ys[y + 1];
        vertex_Ys[2] = subQuad.c.vertex_Ys[y + 1];
        vertex_Ys[3] = subQuad.d.vertex_Ys[y + 1];
        vertex_Ys[4] = subQuad.a.vertex_Ys[y];
        vertex_Ys[5] = subQuad.b.vertex_Ys[y];
        vertex_Ys[6] = subQuad.c.vertex_Ys[y];
        vertex_Ys[7] = subQuad.d.vertex_Ys[y];

        foreach (Vertex_Y vertex_Y in vertex_Ys)
        {
            vertex_Y.subQuad_Cubes.Add(this);
        }
    }

    public void UpdateBit()
    {
        pre_bit = bit;
        string result = "";
        if (vertex_Ys[0].isActive) 
            result += "1";
        else result += "0";
        if (vertex_Ys[1].isActive) 
            result += "1";
        else result += "0";
        if (vertex_Ys[2].isActive)
            result += "1";
        else result += "0";
        if (vertex_Ys[3].isActive) 
            result += "1";
        else result += "0";
        if (vertex_Ys[4].isActive) 
            result += "1";
        else result += "0";
        if (vertex_Ys[5].isActive) 
            result += "1";
        else result += "0";
        if (vertex_Ys[6].isActive) 
            result += "1";
        else result += "0";
        if (vertex_Ys[7].isActive) 
            result += "1";
        else result += "0";

        bit = result;

    }
}
