using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubQuad
{
    public readonly Vertex_Hex a;
    public readonly Vertex_Mid b;
    public readonly Vertex_Center c;
    public readonly Vertex_Mid d;


    public List<SubQuad_Cube> subQuad_Cubes = new List<SubQuad_Cube>();
    public SubQuad(Vertex_Hex a,Vertex_Mid b, Vertex_Center c,Vertex_Mid d,List<SubQuad> subQuads)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;

        subQuads.Add(this);
    }

    /// <summary>
    /// 网格平滑算法
    /// </summary>
    public void CalculateRelaxOffset()
    {
        Vector3 center = (a.currentPosition + b.currentPosition + c.currentPosition + d.currentPosition) / 4;
        
        //a的平滑坐标
        Vector3 vector_a = (a.currentPosition
            + Quaternion.AngleAxis(-90, Vector3.up) * (b.currentPosition - center) + center
            + Quaternion.AngleAxis(-180, Vector3.up) * (c.currentPosition - center) + center
            + Quaternion.AngleAxis(-270, Vector3.up) * (d.currentPosition - center) + center) / 4;

        Vector3 vector_b = Quaternion.AngleAxis(90, Vector3.up) * (vector_a - center) + center;
        Vector3 vector_c = Quaternion.AngleAxis(180, Vector3.up) * (vector_a - center) + center;
        Vector3 vector_d = Quaternion.AngleAxis(270, Vector3.up) * (vector_a - center) + center;

        //偏移值
        a.offset += (vector_a - a.currentPosition) * 0.1f;
        b.offset += (vector_b - b.currentPosition) * 0.1f;
        c.offset += (vector_c - c.currentPosition) * 0.1f;
        d.offset += (vector_d - d.currentPosition) * 0.1f;


    }

    public Vector3 GetCenterPosition()
    {
        return (a.currentPosition + b.currentPosition + c.currentPosition + d.currentPosition) / 4;
    }

    public Vector3 GetMid_ab()
    {
        return (a.currentPosition + b.currentPosition) / 2;
    }
    public Vector3 GetMid_bc()
    {
        return (b.currentPosition + c.currentPosition) / 2;
    }
    public Vector3 GetMid_cd()
    {
        return (c.currentPosition + d.currentPosition) / 2;
    }
    public Vector3 GetMid_ad()
    {
        return (a.currentPosition + d.currentPosition) / 2;
    }

}
