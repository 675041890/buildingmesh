using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


/// <summary>
/// 三角形
/// </summary>
public class Triangle
{
    public readonly Vertex_Hex a;
    public readonly Vertex_Hex b;
    public readonly Vertex_Hex c;

    public readonly Edge ab;
    public readonly Edge bc;
    public readonly Edge ac;

    public readonly Edge[] edges;

    public readonly List<Vertex_Hex> vertices;

    public readonly Vertex_TriangleCenter center;


    public Triangle(Vertex_Hex a,Vertex_Hex b,Vertex_Hex c,List<Edge> edges, List<Triangle> triangles,List<Vertex_Mid> mids, List<Vertex_Center> centers)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        vertices = new List<Vertex_Hex>() { a,b,c };

        ab = Edge.FindEdge(a, b, edges);
        ac = Edge.FindEdge(a, c, edges);
        bc = Edge.FindEdge(b, c, edges);
        if (ab == null)
            ab = new Edge(a, b, edges,mids);
        if (bc == null)
            bc = new Edge(b, c, edges, mids);
        if (ac == null)
            ac = new Edge(a, c, edges, mids);
        this.edges = new Edge[] { ab, bc, ac };

        center = new Vertex_TriangleCenter(this);
        centers.Add(center);

        triangles.Add(this);
    }

    public static void Triangles_Ring(int radius, List<Vertex_Hex> vertices, List<Edge> edges, List<Triangle> triangles, List<Vertex_Mid> mids, List<Vertex_Center> centers)
    {
        List<Vertex_Hex> inner = Vertex_Hex.GrabRing(radius - 1, vertices);
        List<Vertex_Hex> outer = Vertex_Hex.GrabRing(radius, vertices);

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < radius; j++)
            {
                //创建两个顶点在外圈 一个顶点在内圈的三角形
                Vertex_Hex a = outer[i * radius + j];
                Vertex_Hex b = outer[(i * radius + j + 1) % outer.Count];
                Vertex_Hex c = inner[(i * (radius - 1) + j) % inner.Count];
                new Triangle(a, b, c,edges,triangles,mids,centers);
                //创建一个顶点在外圈，两个顶点在内圈的三角形 当j = 0时，即没有内圈
                if (j > 0)
                {
                    Vertex_Hex d = inner[i * (radius - 1) + j - 1];
                    new Triangle(a, c, d,edges,triangles,mids,centers);
                }
            }
        }
    }

    public static void Trigangles_Hex(List<Vertex_Hex> vertices,List<Edge> edges,List<Triangle> triangles, List<Vertex_Mid> mids, List<Vertex_Center> centers)
    {
        for (int i = 1; i <= Grid.radius; i++)
        {
            Triangles_Ring(i,vertices,edges,triangles,mids,centers);
        }
    }


    /// <summary>
    /// 两个三角形是否相邻
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool IsNeighbor(Triangle target)
    {
        HashSet<Edge> intersection = new HashSet<Edge>(edges);
        intersection.IntersectWith(target.edges);
        return intersection.Count == 1;
    }

    public List<Triangle> FindAllNeighborTriangles(List<Triangle> triangles)
    {
        List<Triangle> res = new List<Triangle>();
        foreach (Triangle triangle in triangles)
        {
            if (this.IsNeighbor(triangle))
            {
                res.Add(triangle);
            }
        }
        return res;
    }

    /// <summary>
    /// 相邻三角形共有的边
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public Edge NeighborEdge(Triangle target)
    {
        HashSet<Edge> intersection = new HashSet<Edge>(edges);
        intersection.IntersectWith(target.edges);
        return intersection.Single();
    }

    /// <summary>
    /// 自身未被相邻三角形共用的顶点
    /// </summary>
    /// <param name="neighbor"></param>
    /// <returns></returns>
    public Vertex_Hex IsOlatedVertex_Self(Triangle neighbor)
    {
        HashSet<Vertex_Hex> exception = new HashSet<Vertex_Hex>(vertices);
        exception.ExceptWith(NeighborEdge(neighbor).hexes);
        return exception.Single();
    }

    /// <summary>
    /// 相邻三角形未被共用的顶点
    /// </summary>
    /// <param name="neighbor"></param>
    /// <returns></returns>
    public Vertex_Hex IsOlateVertex_Neighbor(Triangle neighbor)
    {
        HashSet<Vertex_Hex> exception = new HashSet<Vertex_Hex>(neighbor.vertices);
        exception.ExceptWith(NeighborEdge(neighbor).hexes);
        return exception.Single();
    }

    /// <summary>
    /// 合并三角形
    /// </summary>
    /// <param name="neighbor"></param>
    /// <param name="edges"></param>
    /// <param name="triangles"></param>
    /// <param name="quads"></param>
    public void MergeNeighborTriangles(Triangle neighbor, List<Edge> edges,List<Triangle> triangles,List<Quad> quads,List<Vertex_Mid> mids, List<Vertex_Center> centers)
    {

        Vertex_Hex a = IsOlatedVertex_Self(neighbor);
        Vertex_Hex b = vertices[(Array.IndexOf(vertices.ToArray(), a) + 1) % 3];
        Vertex_Hex c = IsOlateVertex_Neighbor(neighbor);
        Vertex_Hex d = neighbor.vertices[(Array.IndexOf(neighbor.vertices.ToArray(), c) + 1) % 3];
        Quad quad = new Quad(a,b,c,d,edges,quads,centers);

        //移除共用边的中点
        mids.Remove(NeighborEdge(neighbor).mid);

        //移除合并的边
        edges.Remove(NeighborEdge(neighbor));

        //合并三角形后移除被合并的两个三角形
        triangles.Remove(neighbor);
        triangles.Remove(this);

        //移除中心点
        centers.Remove(center);
        centers.Remove(neighbor.center);

    }

    /// <summary>
    /// 随机合并相邻的三角形
    /// </summary>
    /// <param name="edges"></param>
    /// <param name="triangles"></param>
    /// <param name="quads"></param>
    public static void RandomlyMergerTriangles(List<Edge> edges,List<Triangle> triangles,List<Quad> quads, List<Vertex_Mid> mids, List<Vertex_Center> centers)
    {
        int randomIndex = UnityEngine.Random.Range(0,triangles.Count);
        List<Triangle> neighbors = triangles[randomIndex].FindAllNeighborTriangles(triangles);
        if (neighbors.Count != 0)
        {
            int randomNeighborIndex = UnityEngine.Random.Range(0, neighbors.Count);
            triangles[randomIndex].MergeNeighborTriangles(neighbors[randomNeighborIndex],edges, triangles,quads,mids,centers);
        }

    }

    /// <summary>
    /// 是否有相邻的三角形
    /// </summary>
    /// <param name="triangles"></param>
    /// <returns></returns>
    public static bool HasNeighborTriangles(List<Triangle> triangles)
    {
        foreach (Triangle a in triangles)
        {
            foreach (Triangle b in triangles)
            {
                if (a.IsNeighbor(b))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void SubDivide(List<SubQuad> subQuads)
    {
        SubQuad quad_a = new SubQuad(a,ab.mid,center,ac.mid, subQuads);
        SubQuad quad_b = new SubQuad(b,bc.mid,center,ab.mid, subQuads);
        SubQuad quad_c = new SubQuad(c,ac.mid,center,bc.mid, subQuads);

        a.subQuads.Add(quad_a);
        b.subQuads.Add(quad_b);
        c.subQuads.Add(quad_c);

        ab.mid.subQuads.Add(quad_a);
        ab.mid.subQuads.Add(quad_b);

        ac.mid.subQuads.Add(quad_c);
        ac.mid.subQuads.Add(quad_a);

        bc.mid.subQuads.Add(quad_c);
        bc.mid.subQuads.Add(quad_b);
    }
}
