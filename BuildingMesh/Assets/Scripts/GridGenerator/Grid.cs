using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid
{
    /// <summary>
    /// 六边形的半径
    /// </summary>
    public static int radius;
    
    public static int height;

    public static float CellSize;

    public static float cellHeight;

    public readonly List<Vertex> vertices = new List<Vertex>();

    public readonly List<Vertex_Hex> hexes = new List<Vertex_Hex>();

    public readonly List<Vertex_Mid> mids = new List<Vertex_Mid>();

    public readonly List<Vertex_Center> centers = new List<Vertex_Center>();

    public readonly List<Edge> edges = new List<Edge>();

    public readonly List<Triangle> triangles = new List<Triangle>();

    public readonly List<Quad> quads = new List<Quad>();

    public readonly List<SubQuad> subQuads = new List<SubQuad>();


    public Grid(int radius,int height, float cellSize, float cellHeight,int relaxTimes)
    {
        Grid.radius = radius;
        Grid.height = height;
        Grid.CellSize = cellSize;
        Grid.cellHeight = cellHeight;
        Vertex_Hex.Hex(hexes,radius);
        Triangle.Trigangles_Hex(hexes,edges,triangles,mids,centers);
        while(Triangle.HasNeighborTriangles(triangles))
            Triangle.RandomlyMergerTriangles(edges,triangles,quads,mids,centers);

        vertices.AddRange(hexes);
        vertices.AddRange(mids);
        vertices.AddRange(centers);

        foreach (var item in triangles)
        {
            item.SubDivide(subQuads);
        }
        foreach (var item in quads)
        {
            item.SubDivide(subQuads);
        }

        for (int i = 0; i < relaxTimes; i++)
        {
            foreach (var subQuad in subQuads)
            {
                subQuad.CalculateRelaxOffset();
            }
        
            foreach (var vertex in vertices)
            {
                vertex.Relax();
            }
        
        }

        foreach (var item in vertices)
        {
            item.index = vertices.IndexOf(item);
            item.BoundaryCheck();
            for (int i = 0; i <= Grid.height; i++)
            {
                item.vertex_Ys.Add(new Vertex_Y(item,i));
            }
        }

        foreach (var subQuad in subQuads)
        {
            for (int i = 0; i < Grid.height; i++)
            {
                subQuad.subQuad_Cubes.Add(new SubQuad_Cube(subQuad, i));
            }
        }
    }
}
