using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 大部分模块旋转90，180，270后和原来不同 有4块 然后x轴镜像 再旋转 可以获得8块
/// 模块Z旋转180和原来相同 2块
/// 模块X轴镜像与原来不同 Z轴旋转180之后不同 
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObject/ModuleLibrary")]
public class ModuleLibrary : ScriptableObject
{
    [SerializeField] 
    private GameObject importModules;

    private Dictionary<string, List<Module>> moduleLibrary = new Dictionary<string, List<Module>>();


    private void Awake()
    {
        ImportedModule(); 
    }

    public void ImportedModule()
    {
        for (int i = 1; i < 256; i++)
        {
            moduleLibrary.Add(Convert.ToString(i,2).PadLeft(8,'0'),new List<Module>());
        }
        foreach (Transform child in importModules.transform)
        {
            Mesh mesh = child.GetComponent<MeshFilter>().sharedMesh;
            string name = child.name;
            name = name.Substring(0, 4) + name.Substring(5,4);
            moduleLibrary[name].Add(new Module(name, mesh, 0, false));
            if (!RotationEqualCheck(name))
            {
                moduleLibrary[RotateName(name, 1)].Add(new Module(RotateName(name,1),mesh,1,false));
                if (!RotationTwiceEqualCheck(name))
                {
                    moduleLibrary[RotateName(name, 2)].Add(new Module(RotateName(name, 2), mesh, 2, false));
                    moduleLibrary[RotateName(name, 3)].Add(new Module(RotateName(name, 3), mesh, 3, false));
                    if (!FlipRotationEqualCheck(name))
                    {
                        moduleLibrary[FlipName(name)].Add(new Module(FlipName(name),mesh,0,true));
                        moduleLibrary[RotateName(FlipName(name), 1)].Add(new Module(RotateName(FlipName(name),1),mesh,1,true));
                        moduleLibrary[RotateName(FlipName(name), 2)].Add(new Module(RotateName(FlipName(name),2),mesh,2,true));
                        moduleLibrary[RotateName(FlipName(name), 3)].Add(new Module(RotateName(FlipName(name),3),mesh,3,true));
                    }
                }
            }
        }
    }

    /// <summary>
    /// 旋转，相当于第四位和第八位移动到第一位和第五位 ，其他位置后移
    /// </summary>
    /// <param name="name"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    private string RotateName(string name,int time)
    {
        string result = name;
        for (int i = 0; i < time; i++)
        {
            result = result[3] + result.Substring(0, 3) + result[7] + result.Substring(4, 3);
        }
        return result;
    }

    private string FlipName(string name)
    {
        return name[3].ToString() + name[2] + name[1] + name[0] + name[7] + name[6] + name[5] + name[4];
    }

    /// <summary>
    /// 旋转后是否相同
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private bool RotationEqualCheck(string name)
    {
        return name[0] == name[1] && name[1] == name[2] && name[2] == name[3]
            && name[3] == name[4] && name[4] == name[5] && name[5] == name[6]
            && name[6] == name[7];
    }

    private bool RotationTwiceEqualCheck(string name)
    {
        return name[0] == name[2] && name[1] == name[3]
            && name[4] == name[6] && name[5] == name[7];
    }

    private bool FlipRotationEqualCheck(string name)
    {
        string symmetry_vertical = name[3].ToString() + name[2] + name[1] + name[0] + name[7] + name[6] + name[5] + name[4];
        string symmetry_horizontal = name[1].ToString() + name[0] + name[3] + name[2] + name[5] + name[4] + name[7] + name[6];
        string symmetry_01 = name[0].ToString() + name[3] + name[2] + name[1] + name[4] + name[7] + name[6] + name[5];
        string symmetry_02 = name[2].ToString() + name[1] + name[0] + name[3] + name[6] + name[5] + name[4] + name[7];
        return name == symmetry_vertical || name == symmetry_horizontal || name == symmetry_01 || name == symmetry_02;
    }

    public List<Module> GetModules(string name)
    {
        List<Module> result;
        if (moduleLibrary.ContainsKey(name))
        {
            result = moduleLibrary[name];
            return result;
        }
        return null;
    }
}
