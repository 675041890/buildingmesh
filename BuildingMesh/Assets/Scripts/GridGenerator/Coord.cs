using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// qrs坐标系（cube坐标系） 
/// </summary>

public class Coord
{
    public readonly int q;
    public readonly int r;
    public readonly int s;
    public readonly int radius;
    public readonly Vector3 WorldPostion;

    public Coord(int q, int r, int s)
    {
        this.q = q;
        this.r = r;
        this.s = s;
        this.radius = Mathf.Max(Mathf.Abs(q), Mathf.Abs(r), Mathf.Abs(s));
        WorldPostion = CoordConvertWorldPositon();
    }

    public Vector3 CoordConvertWorldPositon()
    {
        return new Vector3(q * Mathf.Sqrt(3)/2,0,-(float)r - ((float)q/2)) * 2 * Grid.CellSize;
    }

    /// <summary>
    /// 六边形生成的方向 unity逆时针渲染顶点
    /// </summary>
    public static Coord[] directions = new Coord[]
    {
        new Coord(0,1,-1),
        new Coord(-1,1,0),
        new Coord(-1,0,1),
        new Coord(0,-1,1),
        new Coord(1,-1,0),
        new Coord(1,0,-1),
    };

    public static Coord Direction(int direction)
    {
        return Coord.directions[direction];
    }

    public Coord Add(Coord coord)
    {
        return new Coord(q + coord.q, r + coord.r, s + coord.s);
    }

    /// <summary>
    /// 获得相邻的hex坐标
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public Coord Neighbor(int direction)
    {
        return Add(Direction(direction));
    }

    public Coord Scale(int scale)
    {
        return new Coord(q * scale, r * scale, s * scale);
    }

    /// <summary>
    /// 开始构建单环
    /// </summary>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static List<Coord> Coord_Ring(int radius)
    {
        List<Coord> res = new List<Coord>();
        if (radius == 0)
        {
            res.Add(new Coord(0,0,0)); //六边形的中心点
        }
        Coord hex = Coord.Direction(4).Scale(radius);
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < radius; j++)
            {
                res.Add(hex);
                hex = hex.Neighbor(i);
            }
        }
        return res;
    }

    /// <summary>
    /// 构建六边形点阵
    /// </summary>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static List<Coord> Coord_Hex(int radius)
    {
        List<Coord> result = new List<Coord>();
        for (int i = 0; i <= radius; i++)
        {
            result.AddRange(Coord_Ring(i));
        }
        return result;
    }

}
