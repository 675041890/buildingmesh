using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    [SerializeField]
    private int relaxTimes;

    [SerializeField]
    private int radius;

    [SerializeField]
    private float cellSize;

    [SerializeField]
    private int height;

    [SerializeField]
    private float cellHeight;

    [SerializeField]
    private ModuleLibrary moduleLibrary;

    [SerializeField]
    private Material moduleMaterial;

    private Grid grid;

    public Transform addSphere;

    public Transform deleteSphere;

    void Awake()
    {
        grid = new Grid(radius,height, cellSize,cellHeight, relaxTimes);
        moduleLibrary = Instantiate(moduleLibrary);
    }

    void Update()
    {

        //if (relaxTimes > 0)
        //{
        //    foreach (var subQuad in grid.subQuads)
        //    {
        //        subQuad.CalculateRelaxOffset();
        //    }
        //
        //    foreach (var vertex in grid.vertices)
        //    {
        //        vertex.Relax();
        //    }
        //    relaxTimes--;
        //}

        foreach (Vertex vertex in grid.vertices)
        {
            foreach (Vertex_Y vertex_y in vertex.vertex_Ys)
            {
                if (!vertex_y.isActive && Vector3.Distance(vertex_y.worldPosition,addSphere.position) < 1f && !vertex_y.isBoundary)
                {
                    vertex_y.isActive = true;
                }
                else if (vertex_y.isActive && Vector3.Distance(vertex_y.worldPosition,deleteSphere.position) < 2f)
                {
                    vertex_y.isActive = false;
                }
            }
        }

        foreach (SubQuad subQuad in grid.subQuads)
        {
            foreach (SubQuad_Cube subQuad_Cube in subQuad.subQuad_Cubes)
            {
                subQuad_Cube.UpdateBit();
                if (subQuad_Cube.pre_bit != subQuad_Cube.bit)
                {
                    UpdateSlot(subQuad_Cube);
                }
            }
          
        }
    }

    private void UpdateSlot(SubQuad_Cube subQuad_Cube)
    {
        string name = "Slot_" + grid.subQuads.IndexOf(subQuad_Cube.subQuad) + "_" + subQuad_Cube.y;
        GameObject slot_GameObject;
        if (transform.Find(name))
        {
            slot_GameObject = transform.Find(name).gameObject;
        }
        else
            slot_GameObject = null;

        if (slot_GameObject == null)
        {
            if (subQuad_Cube.bit != "00000000" && subQuad_Cube.bit != "11111111")
            {
                slot_GameObject = new GameObject(name, typeof(Slot));
                slot_GameObject.transform.SetParent(transform);
                slot_GameObject.transform.localPosition = subQuad_Cube.centerPosition; //导致生成模块位于地面的cellhight * 0.5处
                Slot slot = slot_GameObject.GetComponent<Slot>();
                slot.Initialize(moduleLibrary, subQuad_Cube, moduleMaterial);
                slot.UpdateModule(slot.possibleModules[0]);
            }

        }
        else
        {
            Slot slot = slot_GameObject.GetComponent<Slot>();
            if (subQuad_Cube.bit == "00000000" || subQuad_Cube.bit == "11111111")
            {
                Destroy(slot_GameObject);
                Resources.UnloadUnusedAssets();
            }
            else
            {
                slot.ResetPossibleModules(moduleLibrary);
                slot.UpdateModule(slot.possibleModules[0]);
            }
        }
    }

    public void ToggleSlot(Vertex_Y vertex_Y)
    {
        vertex_Y.isActive = !vertex_Y.isActive;
        foreach (SubQuad subQuad in grid.subQuads)
        {
            foreach (SubQuad_Cube subQuad_Cube in subQuad.subQuad_Cubes)
            {
                subQuad_Cube.UpdateBit();
                if (subQuad_Cube.pre_bit != subQuad_Cube.bit)
                {
                    UpdateSlot(subQuad_Cube);
                }
            }

        }
    }

    public Grid GetGrid()
    {
        return grid;
    }
    private void OnDrawGizmos()
    {
        if(grid == null) 
            return;

        #region 二维
        //foreach (Vertex_Hex vertex_Hex in grid.hexes)
        //{
        //    Gizmos.DrawSphere(vertex_Hex.currentPosition, 0.1f);
        //}
        //Gizmos.color = Color.yellow;
        //foreach (Triangle item in grid.triangles)
        //{
        //    Gizmos.DrawLine(item.a.currentPosition, item.b.currentPosition);
        //    Gizmos.DrawLine(item.a.currentPosition, item.c.currentPosition);
        //    Gizmos.DrawLine(item.c.currentPosition, item.b.currentPosition);
        //    Gizmos.DrawSphere((item.a.currentPosition + item.b.currentPosition + item.c.currentPosition) / 3, 0.1f);
        //}

        //Gizmos.color = Color.green;
        //foreach (var item in grid.quads)
        //{
        //    Gizmos.DrawLine(item.a.currentPosition, item.b.currentPosition);
        //    Gizmos.DrawLine(item.a.currentPosition, item.d.currentPosition);
        //    Gizmos.DrawLine(item.b.currentPosition, item.c.currentPosition);
        //    Gizmos.DrawLine(item.c.currentPosition, item.d.currentPosition);
        //}

        //Gizmos.color = Color.red;
        //foreach (var item in grid.mids)
        //{
        //    Gizmos.DrawSphere(item.currentPosition, 0.1F);
        //}

        //Gizmos.color = Color.blue;
        //foreach (var item in grid.centers)
        //{
        //    Gizmos.DrawSphere(item.currentPosition, 0.3f);
        //}

        //Gizmos.color = Color.white;
        //foreach (var item in grid.subQuads)
        //{
        //    Gizmos.DrawLine(item.a.currentPosition, item.b.currentPosition);
        //    Gizmos.DrawLine(item.b.currentPosition, item.c.currentPosition);
        //    Gizmos.DrawLine(item.c.currentPosition, item.d.currentPosition);
        //    Gizmos.DrawLine(item.d.currentPosition, item.a.currentPosition);
        //}
        #endregion

        //Gizmos.color = Color.yellow;
        //foreach (var vertex in grid.vertices)
        //{
        //    foreach (var vertex_y in vertex.vertex_Ys)
        //    {
        //        if (vertex_y.isActive)
        //            Gizmos.color = Color.red;
        //        else
        //            Gizmos.color = Color.gray;

        //        Gizmos.DrawSphere(vertex_y.worldPosition, 0.1f);
        //    }
        //}

        //foreach (SubQuad subQuad in grid.subQuads)
        //{
        //    foreach (SubQuad_Cube subQuad_Cube in subQuad.subQuad_Cubes)
        //    {
        //        Gizmos.color = Color.red;
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[0].worldPosition, subQuad_Cube.vertex_Ys[1].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[1].worldPosition, subQuad_Cube.vertex_Ys[2].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[2].worldPosition, subQuad_Cube.vertex_Ys[3].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[3].worldPosition, subQuad_Cube.vertex_Ys[0].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[4].worldPosition, subQuad_Cube.vertex_Ys[5].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[5].worldPosition, subQuad_Cube.vertex_Ys[6].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[6].worldPosition, subQuad_Cube.vertex_Ys[7].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[7].worldPosition, subQuad_Cube.vertex_Ys[4].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[0].worldPosition, subQuad_Cube.vertex_Ys[4].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[1].worldPosition, subQuad_Cube.vertex_Ys[5].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[2].worldPosition, subQuad_Cube.vertex_Ys[6].worldPosition);
        //        Gizmos.DrawLine(subQuad_Cube.vertex_Ys[3].worldPosition, subQuad_Cube.vertex_Ys[7].worldPosition);

        //        //Debug.Log(Vector3.Distance(subQuad_Cube.vertex_Ys[0].worldPosition, subQuad_Cube.vertex_Ys[1].worldPosition));
        //        Gizmos.color = Color.blue;
        //        Gizmos.DrawSphere(subQuad_Cube.centerPosition,0.01f);
        //        GUI.color = Color.blue;
        //        Handles.Label(subQuad_Cube.centerPosition, subQuad_Cube.bit);
        //        //Handles.Label(subQuad_Cube.centerPosition, subQuad_Cube.centerPosition.ToString());

        //    }
        //}
    }
}
