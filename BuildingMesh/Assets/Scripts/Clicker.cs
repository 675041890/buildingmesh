using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public enum RayCastHitType
{
    None,
    Ground,
    Top,
    Bottom,
    Side,

    
}

public class Clicker : MonoBehaviour
{
    private GridGenerator gridGenerator;

    private ColliderSystem colliderSystem;

    private SlotColliderSystem slotColliderSystem;

    private PlayerInputAction inputActions;

    private RayCastHitType rayCastHitType;

    [SerializeField]
    private RaycastHit raycastHit;

    [SerializeField]
    private float raycastRange;

    [SerializeField]
    private LayerMask clickerLayerMask;

    [SerializeField]
    private Cursor cursor;

    private Vertex_Y vertex_Y_Selected;

    private Vertex_Y pre_vertex_Y_Selected;

    private Vertex_Y vertex_Y_Target;

    private Vertex_Y pre_vertex_Y_Target;



    private void Awake()
    {
        gridGenerator = GetComponentInParent<WorldMaster>().gridGenerator;
        colliderSystem = GetComponentInParent<WorldMaster>().colliderSystem;
        slotColliderSystem = colliderSystem.GetSlotColliderSystem();

        inputActions = new PlayerInputAction();
        inputActions.Build.Enable();
        inputActions.Build.Add.performed += Add;
        inputActions.Build.Delete.performed += Delete;
    }

    private void Update()
    {
        FindTarget();
        UpdateCursor();
    }

    private void FindTarget()
    {
        pre_vertex_Y_Selected = vertex_Y_Selected;
        pre_vertex_Y_Target = vertex_Y_Target;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray,out raycastHit,raycastRange, clickerLayerMask))
        {
            if (raycastHit.transform.GetComponent<GroundCollider_Quad>())
            {
                vertex_Y_Selected = null;

                //计算目标
                Vector3 aim = raycastHit.point;
                SubQuad subQuad = raycastHit.transform.GetComponent<GroundCollider_Quad>().subQuad;

                Vector3 a = subQuad.a.currentPosition;
                Vector3 b = subQuad.b.currentPosition;
                Vector3 c = subQuad.c.currentPosition;
                Vector3 d = subQuad.d.currentPosition;

                var ab = (a + b) / 2;
                var cd = (c + d) / 2;
                var bc = (b + c) / 2;
                var ad = (a + d) / 2;
                
                float ab_cd = (aim.z - ab.z) * (aim.x - cd.x) - (aim.z - cd.z) * (aim.x - ab.x);
                float bc_da = (aim.z - bc.z) * (aim.x - ad.x) - (aim.z - ad.z) * (aim.x - bc.x);

                float a_ab_cd = (a.z - ab.z) * (a.x - cd.x) - (a.z - cd.z) * (a.x - ab.x);
                float a_bc_da = (a.z -bc.z) * (a.x - ad.x) - (a.z - ad.z) * (a.x - bc.x);

                bool on_ad_side = ab_cd * a_ab_cd >= 0;
                bool on_ab_side = bc_da * a_bc_da >= 0;

                if (on_ab_side && on_ad_side)
                    vertex_Y_Target = subQuad.a.vertex_Ys[1];
                else if(on_ad_side && !on_ab_side)
                    vertex_Y_Target = subQuad.d.vertex_Ys[1];
                else if (!on_ad_side && on_ab_side)
                    vertex_Y_Target = subQuad.b.vertex_Ys[1];
                else
                    vertex_Y_Target = subQuad.c.vertex_Ys[1];

                if (vertex_Y_Target.vertex.isBoundary)
                {
                    vertex_Y_Target = null;
                    rayCastHitType = RayCastHitType.None;
                }
                else
                {
                    rayCastHitType = RayCastHitType.Ground;
                }
                    
            }
            else
            {
                vertex_Y_Selected = raycastHit.transform.parent.GetComponent<SlotCollider>().vertex_Y;
                int y = vertex_Y_Selected.y;
                if (raycastHit.transform.GetComponent<SlotCollider_Top>())
                {
                    if (y < Grid.height - 1)
                    {
                        vertex_Y_Target = vertex_Y_Selected.vertex.vertex_Ys[y + 1];
                        rayCastHitType = RayCastHitType.Top;
                    }
                    else
                    {
                        vertex_Y_Target = null;
                        rayCastHitType = RayCastHitType.None;
                    }
                }
                else if(raycastHit.transform.GetComponent<SlotCollider_Bottom>())
                {
                    if (y > 1)
                    {
                        vertex_Y_Target = vertex_Y_Selected.vertex.vertex_Ys[y - 1];
                        rayCastHitType = RayCastHitType.Bottom;
                    }
                    else
                    {
                        vertex_Y_Target = null;
                        rayCastHitType = RayCastHitType.None;
                    }
                }
                else
                {
                    vertex_Y_Target = raycastHit.transform.GetComponent<SlotCollider_Side>().neighbor;
                    if (vertex_Y_Target.vertex.isBoundary)
                    {
                        vertex_Y_Target = null;
                        rayCastHitType = RayCastHitType.None;
                    }
                    else
                    {
                        rayCastHitType = RayCastHitType.Side;
                    }
                }
            }
        }
        else
        {
            vertex_Y_Target = null;
            vertex_Y_Selected = null;
            rayCastHitType = RayCastHitType.None;
        }
    }

    private void UpdateCursor()
    {
        if(pre_vertex_Y_Selected != vertex_Y_Selected || pre_vertex_Y_Target != vertex_Y_Target)
            cursor.UpdateCursor(raycastHit,rayCastHitType, vertex_Y_Selected, vertex_Y_Target);
    }

    /// <summary>
    /// p1在p0p2的什么方向
    /// p0p1向量 和 p0p2向量
    /// 结果为整，p1在p0p2的顺时针方向 否则逆时针方向
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="p0"></param>
    /// <returns></returns>
    double Cross(Vector3 p1,Vector3 p2,Vector3 p0)
    {
  
        return (p1.x - p0.x) * (p2.z - p0.z) - (p1.z - p0.z) * (p2.x - p0.x);
    }

    private void Add(InputAction.CallbackContext ctx)
    {
        if(vertex_Y_Target != null && !vertex_Y_Target.isActive)
        {
            gridGenerator.ToggleSlot(vertex_Y_Target);
            slotColliderSystem.CreateCollider(vertex_Y_Target);
        }

    }

    private void Delete(InputAction.CallbackContext ctx)
    {
        if (vertex_Y_Selected != null && vertex_Y_Selected.isActive)
        {
            gridGenerator.ToggleSlot(vertex_Y_Selected);
            slotColliderSystem.DestroyCollider(vertex_Y_Selected);
        }

    }

    private void OnDrawGizmos()
    {
        if (vertex_Y_Target != null)
        {
            Gizmos.DrawSphere(vertex_Y_Target.worldPosition,0.2f);
        }
    }
}
