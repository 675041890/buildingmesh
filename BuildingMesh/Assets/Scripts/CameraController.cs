using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private PlayerInputAction inputActions;

    private Vector3 currentMovementInput;

    private Vector3 currentRoationInput;

    [SerializeField]
    private float speed;
    [SerializeField]
    private float RotationSpeed;
    [SerializeField]
    private bool tryRotate;

    private void Awake()
    {
        inputActions = new PlayerInputAction();
        inputActions.Build.Enable();
        inputActions.Build.Move.performed += ctx => currentMovementInput = ctx.ReadValue<Vector3>();
        inputActions.Build.Move.canceled += ctx => currentMovementInput = Vector3.zero;

        inputActions.Build.Rotate.performed += ctx => currentRoationInput = ctx.ReadValue<Vector2>();
        inputActions.Build.Rotate.canceled += ctx => currentRoationInput = Vector2.zero;

        inputActions.Build.TryRotate.performed += ctx => tryRotate = true ;
        inputActions.Build.TryRotate.canceled += ctx => tryRotate = false;
    }

    private void Move()
    {
        Vector3 inputValue = currentMovementInput.x * transform.right + currentMovementInput.y * Vector3.up + currentMovementInput.z * transform.forward;
        transform.localPosition += inputValue * Time.deltaTime * speed;
            
    }

    private void Rotate()
    {
        if (tryRotate)
            transform.rotation = Quaternion.Euler(currentRoationInput.y * Time.deltaTime * RotationSpeed + transform.rotation.eulerAngles.x, currentRoationInput.x * Time.deltaTime * RotationSpeed + transform.rotation.eulerAngles.y,0f);

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Rotate();
    }
}
