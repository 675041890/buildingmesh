using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// 鼠标位置的显示UI
/// </summary>
public class Cursor : MonoBehaviour
{
    private void Awake()
    {
        this.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
    }

    public void UpdateCursor(RaycastHit raycastHit,RayCastHitType rayCastHitType, Vertex_Y selected,Vertex_Y target)
    {
        
        GetComponent<MeshFilter>().mesh.Clear();
        if (rayCastHitType == RayCastHitType.Ground)
        {
            Vertex vertex = target.vertex;
            GetComponent<MeshFilter>().mesh = vertex.CreateMesh();
            transform.position = target.worldPosition + Vector3.down * Grid.cellHeight * 0.5f;
        } 
        else if(rayCastHitType == RayCastHitType.Top)
        {
            Vertex vertex = selected.vertex;
            GetComponent<MeshFilter>().mesh = vertex.CreateMesh();
            transform.position = selected.worldPosition + Vector3.up * Grid.cellHeight * 0.6f;
        }
        else if (rayCastHitType == RayCastHitType.Bottom)
        {
            Vertex vertex = selected.vertex;
            GetComponent<MeshFilter>().mesh = vertex.CreateMesh();
            GetComponent<MeshFilter>().mesh.triangles = GetComponent<MeshFilter>().mesh.triangles.Reverse().ToArray();
            transform.position = selected.worldPosition + Vector3.down * Grid.cellHeight * 0.6f;
        }
        else if(rayCastHitType == RayCastHitType.Side)
        {
            Vertex vertex = selected.vertex;
            GetComponent<MeshFilter>().mesh = raycastHit.transform.GetComponent<MeshCollider>().sharedMesh;
            transform.position = selected.worldPosition;
        }

    }
}
